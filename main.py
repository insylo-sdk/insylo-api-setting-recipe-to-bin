# ____                        _           _    ____ ___
#/ ___|  __ _ _ __ ___  _ __ | | ___     / \  |  _ \_ _|
#\___ \ / _` | '_ ` _ \| '_ \| |/ _ \   / _ \ | |_) | |   _____
# ___) | (_| | | | | | | |_) | |  __/  / ___ \|  __/| |  |_____|
#|____/ \__,_|_| |_| |_| .__/|_|\___| /_/   \_\_|  |___|
#                      |_|
# _   _                 _                   _                 _ _      _
#| | | | _____      __ | |_ ___    ___  ___| |_    __ _    __| (_) ___| |_
#| |_| |/ _ \ \ /\ / / | __/ _ \  / __|/ _ \ __|  / _` |  / _` | |/ _ \ __|
#|  _  | (_) \ V  V /  | || (_) | \__ \  __/ |_  | (_| | | (_| | |  __/ |_
#|_| |_|\___/ \_/\_/    \__\___/  |___/\___|\__|  \__,_|  \__,_|_|\___|\__|


import pprint
import datetime
from dateutil.parser import parse
import pandas as pd
from pandas.plotting import register_matplotlib_converters
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from insyloapi import InsyloAPI

register_matplotlib_converters()

username = "XXX"
password = "XXX"
    
if __name__ == "__main__":
    # Two environments available (Production and Sandbox environments)
    #environment = 'apis.insylo.io'
    environment = 'dev.apis.insylo.io'

    # Connect to the API and retrieve a new token
    insylo = InsyloAPI(username, password, environment)

    # ALERT: REUSE THE GIVEN TOKEN, DO NOT RECONNECT AGAIN AND AGAIN, YOU WILL BE BANNED.
    # TOKEN EXPIRES IN 30 DAYS.

    # Set a data time window to work with
    end_date_d = datetime.datetime(2021, 5, 9, 00, 00, 00)
    start_date_d = end_date_d + datetime.timedelta(days=-6)

    ##############################################    
    #  ____           _
    # |  _ \ ___  ___(_)_ __   ___  ___
    # | |_) / _ \/ __| | '_ \ / _ \/ __|
    # |  _ <  __/ (__| | |_) |  __/\__ \
    # |_| \_\___|\___|_| .__/ \___||___/
    #                  |_|
                 
    # Get available diets
    diets = insylo.get_diets()
    dietsList = [[diet['id'], diet['label'],float(diet['density'])] for diet in diets['data']]
    pprint.pprint(dietsList)

    recipeId = '5e285ab82e032a38ccf8f971'
    # We select a recipe to set and build the payload
    selectedRecipe = {"recipeId": recipeId}
    #selectedRecipe = {"recipeId": dietsList[0][0]}
    
    ##############################################    
    # _                                         _
    #| |    ___   ___  _ __     ___  _ __      / \   _ __ ___  __ _ ___
    #| |   / _ \ / _ \| '_ \   / _ \| '_ \    / _ \ | '__/ _ \/ _` / __|
    #| |__| (_) | (_) | |_) | | (_) | | | |  / ___ \| | |  __/ (_| \__ \
    #|_____\___/ \___/| .__/   \___/|_| |_| /_/   \_\_|  \___|\__,_|___/
    #                 |_|

    # We could apply to a group of bins by looping on areas
    # List all the areas (aka. farms).
    areas = insylo.get_areas()
    binUUID = ''
    
    # Get bins from each area
    for area in areas["data"]:
        # We're gonna set the same diet for every bin from this farm
        bins = insylo.get_bins(area["id"])
        try:
            # Here we just print some information from farm -> bin relationship.
            for bi in bins["data"]:
                print("{} :: {} :: {} :: {}".format(bi['uuid'], area['label'],area['description'], bi['label']))
                binUUID = bi['uuid']
        except:
            print('No bins')
            
    ##############################################    
    # ____       _            _               __ _ _ _ _
    #|  _ \  ___| |_ ___  ___| |_   _ __ ___ / _(_) | (_)_ __   __ _
    #| | | |/ _ \ __/ _ \/ __| __| | '__/ _ \ |_| | | | | '_ \ / _` |
    #| |_| |  __/ ||  __/ (__| |_  | | |  __/  _| | | | | | | | (_| |
    #|____/ \___|\__\___|\___|\__| |_|  \___|_| |_|_|_|_|_| |_|\__, |
    #                                                          |___/    
                
    # Get reading from a given bin and a time window
    binUUID = 'BINF66B6E77-39EF-42C2-82AC-B3484I9226NE'
    readings = insylo.get_metrics(binUUID, start_date_d, end_date_d)
    # Get all readings from the previous N days
    
    ts = [parse(sample['timestamp']['createdAt']) for sample in readings['data']]

    weights = [[sample['id'], 
                float(sample['weight'])] for sample in readings['data']]
    
    #Create DataFrame to plot timeserie
    data = pd.DataFrame(weights, columns=['id', 'weight'], index = ts)
    verifiedData = data['weight']!=0
    data = data[verifiedData]

    ##############################################
    # Detect refillings greated that maxLoad tonnes
    # assuming hourly readings, direct substraction between 
    # current and previous data, give us a feed variation.  
    maxLoad = 1.0                    
    # Every variation larger than maxLoad will be accepted as a refilling
    data['lag_1'] = data.weight.shift(-1)
    data['lag_1'] = data.lag_1.fillna(method='backfill')
    data['diff']  = data.weight - data.lag_1
    isLoad = data['diff']>maxLoad
    data['stock'] = data[isLoad]['weight']
    data.sort_index(ascending=False, inplace=True)
            
    ##############################################
    # Plot time series to validate refilling detection
    fig = plt.figure(figsize=(8, 6), dpi=100)
    ax = fig.add_subplot(111)
    data['weight'].plot(fig=fig, ax=ax, style='k')
    data['stock'].plot(fig=fig, ax=ax, style='r+')
    ax.set_xlabel('Date')
    ax.set_ylabel('Bin stock (Tn)')
    # set major ticks format
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%b %d'))
    plt.show()
    plt.close()
            
    ##############################################    
    # ____       _         _ _      _
    #/ ___|  ___| |_    __| (_) ___| |_
    #\___ \ / _ \ __|  / _` | |/ _ \ __|
    # ___) |  __/ |_  | (_| | |  __/ |_
    #|____/ \___|\__|  \__,_|_|\___|\__|
    
    # Given a datapoint
    loadEvent = data[data['stock'].notna()]
    pprint.pprint(loadEvent)
    for dataId in loadEvent['id'].values:                        
        # Set recipe into refilling data point
        print("Datapoint: {}".format(dataId))
        response = insylo.set_diet(dataId, selectedRecipe)
        print(response)
